<?php

namespace App\Http\Controllers;

use App\Repositories\OrderRepository;
use Illuminate\Routing\Controller as BaseController;
use App\Repositories\CustomerRepository;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class CustomerDetailsController
 */
class CustomerDetailsController extends BaseController
{
    /**
     * @param $id
     * @param CustomerRepository $customerRepository
     * @return View
     */
    public function show($id, CustomerRepository $customerRepository)
    {
        return view('customer-details', ['customer' => $customerRepository->find((int)$id, ['lifetime_value'])]);
    }

    /**
     * @param Request $request
     * @param string $id
     * @param OrderRepository $orderRepository
     * @return View
     */
    public function orders(Request $request, string $id, OrderRepository $orderRepository)
    {
        $orders = $orderRepository->list([
            'page' => ($request->start / $request->length) + 1,
            'limit' => $request->length,
            'customer_id' => $id
        ]);

        return view('orders-list', [
            'request' => $request,
            'orders' => $orders,
            'total_count' => $orderRepository->getCount(['customer_id' => $id]),
        ]);
    }
}
