<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use App\Repositories\CustomerRepository;
use Illuminate\View\View;

/**
 * Class CustomersController
 */
class CustomersController extends BaseController
{
    /**
     * @return View
     */
    public function index()
    {
        return view('customers-index');
    }

    /**
     * @param Request $request
     * @param CustomerRepository $customerRepository
     * @return View
     */
    public function list(Request $request, CustomerRepository $customerRepository)
    {
        $customers = $customerRepository->list([
            'page' => ($request->start / $request->length) + 1,
            'limit' => $request->length,
        ], ['orders_count']);

        return view('customers-list', [
            'request' => $request,
            'customers' => $customers,
            'total_count' => $customerRepository->getCount()
        ]);
    }
}
