<?php

namespace App\Repositories;

use Bigcommerce\Api\Resources\Order;

/**
 * Class OrderRepository
 */
class OrderRepository extends ResourceRepository
{
    /**
     * @param array $filter
     * @return Order[]
     */
    protected function getResourceCollection(array $filter = [])
    {
        return $this->bigcommerce->getOrders($filter);
    }

    /**
     * @param int $id
     * @return Order
     */
    protected function findBaseResource(int $id)
    {
       return $this->bigcommerce->getOrder($id);
    }

    /**
     * @param array $filter
     * @return int
     */
    public function getCount(array $filter = [])
    {
        return $this->bigcommerce->getOrdersCount($filter);
    }
}