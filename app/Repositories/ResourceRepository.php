<?php

namespace App\Repositories;

use Bigcommerce\Api\Resource;
use Bigcommerce\Api\Client as Bigcommerce;

/**
 * Class ResourceRepository
 */
abstract class ResourceRepository
{
    /**
     * @var Bigcommerce
     */
    protected $bigcommerce;

    /**
     * ResourceRepository constructor.
     * @param Bigcommerce $bigcommerce
     */
    public function __construct(Bigcommerce $bigcommerce)
    {
        $this->bigcommerce = $bigcommerce;
    }

    /**
     * Returns an array of resources
     *
     * @param array $filter Filtering criteria.
     * @param array $extraFields Additional fields to calculate and add to resources.
     *
     * @return Resource[]
     */
    public function list(array $filter = [], array $extraFields = [])
    {
        $collection = $this->getResourceCollection($filter);

        if (is_null($collection)) {
            return [];
        }

        foreach ($collection as $resource) {
            $this->addFields($resource, $extraFields);
        }

        return $collection;
    }

    /**
     * Finds a resource
     *
     * @param int $id Resource id.
     * @param array $extraFields Additional fields to calculate and add to resource.
     *
     * @return Resource
     */
    public function find(int $id, array $extraFields = [])
    {
        $resource = $this->findBaseResource($id);

        if (!$resource) {
            return null;
        }

        $this->addFields($resource, $extraFields);

        return $resource;
    }

    /**
     * Calculates and add additional fields to a resource
     *
     * Should be overwritten in subclasses if needed.
     *
     * @param Resource $resource
     * @param array $fields
     *
     * @return Resource
     */
    public function addFields(Resource $resource, array $fields)
    {
        return $resource;
    }

    /**
     * Returns the total count of resources
     * @param array $filter
     * @return int
     */
    public abstract function getCount(array $filter = []);

    /**
     * Finds a resource
     * @param int $id
     * @return Resource
     */
    protected abstract function findBaseResource(int $id);

    /**
     * Gets resource collection
     *
     * @param array $filter
     * @return Resource[]
     */
    protected abstract function getResourceCollection(array $filter = []);
}