<?php

namespace App\Repositories;

use Bigcommerce\Api\Client as Bigcommerce;
use Bigcommerce\Api\Resource;
use Bigcommerce\Api\Resources\Customer;

/**
 * Class CustomerRepository
 */
class CustomerRepository extends ResourceRepository
{
    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * CustomerRepository constructor.
     * @param Bigcommerce $bigcommerce
     * @param OrderRepository $orderRepository
     */
    public function __construct(Bigcommerce $bigcommerce, OrderRepository $orderRepository)
    {
        parent::__construct($bigcommerce);
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param array $filter
     * @return int
     */
    public function getCount(array $filter = [])
    {
        return $this->bigcommerce->getCustomersCount($filter);
    }

    /**
     * @param array $filter
     * @return Resource[]
     */
    protected function getResourceCollection(array $filter = [])
    {
        return $this->bigcommerce->getCustomers($filter);
    }

    /**
     * @param int $id
     * @return Customer
     */
    protected function findBaseResource(int $id)
    {
        return $this->bigcommerce->getCustomer($id);
    }

    /**
     * @param Resource $resource
     * @param string[] $fields
     * @return Resource
     */
    public function addFields(Resource $resource, array $fields)
    {
        foreach ($fields as $field) {
            switch ($field) {
                case 'orders_count' :
                    $resource->orders_count = $this->bigcommerce->getOrdersCount(['customer_id' => $resource->id]);
                    break;
                case 'lifetime_value':
                    $resource->lifetime_value = $this->getLifetimeValue($resource);
                    break;
            }
        }
        return $resource;
    }

    /**
     * Returns the total value of all of a customer's orders
     *
     * @param Customer $customer
     * @return float
     */
    public function getLifetimeValue(Customer $customer)
    {
        $value = 0.0000;
        $page = 1;
        while (true) {
            $orders = $this->orderRepository->list(['page' => $page, 'customer_id' => $customer->id]);
            if (empty($orders)) {
                break;
            }
            foreach ($orders as $order) {
                $value += $order->total_inc_tax;
            }
            $page++;
        }
        return $value;
    }
}