@extends('layouts.app')

@section('title', $customer->first_name . "'s Order History")

@section('content')
    <table id="table-orders">
        <thead>
            <tr>
                <th>Date</th>
                <th># of Products</th>
                <th>Total</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <td colspan="2">Lifetime Value</td>
                <td>${{ $customer->lifetime_value }}</td>
            </tr>
        </tfoot>
        <tbody>
        </tbody>
    </table>
    @include('datatable', ['tableId' => 'table-orders', 'ajax' => "/customers/$customer->id/orders"])
@endsection
