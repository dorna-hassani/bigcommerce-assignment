<?php
$data = [];
foreach ($orders as $order) {
    $data[] = [
        $order->date_created,
        count($order->products),
        "$" . $order->total_inc_tax,
    ];
}
echo json_encode([
    'draw' => $request->draw,
    'recordsTotal' => $total_count,
    'recordsFiltered' => $total_count,
    'data' => $data,]);