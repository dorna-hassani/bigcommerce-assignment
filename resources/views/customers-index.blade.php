@extends('layouts.app')

@section('title', 'Customers')

@section('content')
    <table id="table-customers">
        <thead>
            <tr>
                <th>Name</th>
                <th># of Orders</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    @include('datatable', ['tableId' => 'table-customers', 'ajax' => 'customers/list'])
@endsection
