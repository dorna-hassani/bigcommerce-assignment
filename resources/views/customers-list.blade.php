<?php
$data = [];
foreach ($customers as $customer) {
    $data[] = [
        "<a href='/customers/{$customer->id}'>$customer->first_name $customer->last_name</a>",
        $customer->orders_count,
    ];
}

echo json_encode([
    'draw' => $request->draw,
    'recordsFiltered' => $total_count,
    'recordsTotal' => $total_count,
    'data' => $data,
]);