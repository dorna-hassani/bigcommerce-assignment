<script type="text/javascript">
    $(document).ready(function() {
        $("#{{$tableId}}").DataTable( {
            "serverSide": true,
            "processing": true,
            "ordering": false,
            "searching": false,
            "pagingType": "full_numbers",
            "ajax": "{{$ajax}}"
        } );
    } );
</script>