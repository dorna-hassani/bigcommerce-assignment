<?php

namespace Tests\Unit;

use Bigcommerce\Api\Resources\Customer;
use Bigcommerce\Api\Resources\Order;
use Tests\TestCase;
use Bigcommerce\Api\Client as Bigcommerce;
use App\Repositories\CustomerRepository;
use App\Repositories\OrderRepository;
use Faker\Factory as FakerFactory;

class CustomerRepositoryTest extends TestCase
{
    /**
     * @throws \ReflectionException
     */
    public function testGetLifetimeValue()
    {
        $orders = [];

        $faker = FakerFactory::create();

        $orders[] = new Order(['total_inc_tax' => $faker->randomFloat(4)]);
        $orders[] = new Order(['total_inc_tax' => $faker->randomFloat(4)]);
        $orders[] = new Order(['total_inc_tax' => $faker->randomFloat(4)]);

        $lifeTimeValue = 0.0000;
        foreach ($orders as $order) {
            $lifeTimeValue += $order->total_inc_tax;
        }

        $orderRepository = $this->createMock(OrderRepository::class);
        $orderRepository->method('list')->will($this->onConsecutiveCalls($orders, null));

        $bigCommerce = $this->createMock(Bigcommerce::class);

        $customerRepository = new CustomerRepository($bigCommerce, $orderRepository);

        $this->assertEquals($lifeTimeValue, $customerRepository->getLifetimeValue(new Customer()));
    }
}
