<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CustomerOrdersTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetCustomerOrders()
    {
        $response = $this->get('/customers/10/orders?page=1&length=10');

        $response->assertStatus(200);

        $response->assertViewIs('orders-list');

        $response->assertJsonStructure(['recordsTotal','data']);

        /**
         * Tests the number of returned orders is correct.
         * Assumption has been made that the store url belongs to a test store with fixed data (fixture).
         * In this assignment's case: https://store-velgoi8q0k.mybigcommerce.com
         */
        $response->assertJsonCount(3,'data');
    }
}
